/**
 * Created by Dmitry on 6/17/2015.
 */
public class Employee {
    private String name;
    private String employeeNumber;
    private String hireDate;

    public Employee(String n, String num, String date){
        name = n;
        setEmployeeNumber(num);
        hireDate = date;
    }

    public Employee(){
        name = "";
        employeeNumber = "";
        hireDate = "";
    }

    public void setName(String n){
        name = n;
    }

    public void setEmployeeNumber(String num) {
        if (isValidEmpNum(num))
            employeeNumber = num;
        else
            employeeNumber = "";
    }

    public void setHireDate(String date) {
        hireDate = date;
    }

    public String getName() {
        return name;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public String getHireDate() {
        return hireDate;
    }

    private boolean isValidEmpNum(String num){
        boolean status = true;

        if (num.length() != 5)
            status = false;
        else {
            if ((!Character.isDigit(num.charAt(0))) ||
                (!Character.isDigit(num.charAt(1))) ||
                (!Character.isDigit(num.charAt(2))) ||
                (num.charAt(3) != '-')              ||
                (Character.toUpperCase(num.charAt(4)) < 'A') ||
                (Character.toUpperCase(num.charAt(4)) > 'M'))
                    status = false;
        }
        return status;
    }

    public String toString(){
        String str = "Name: " + name + "\nEmployee Number: ";

        if (employeeNumber == "")
            str += "Invalid Employee Number";
        else
            str += employeeNumber;

        str += ("\nHire Date: " + hireDate);
        return str;
    }
}
